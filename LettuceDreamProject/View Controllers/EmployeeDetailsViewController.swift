//
//  EmployeeDetailsViewController.swift
//  LettuceDreamProject
//
//  Created by Rohith Bharadwaj on 01/03/20.
//  Copyright © 2020 Bearcat Coders. All rights reserved.
//

import UIKit

class EmployeeDetailsViewController: UIViewController {
    static var name:String = ""
    @IBOutlet weak var EmployeeIDTF: UITextField!
    @IBAction func ProceedToAttendButton(_ sender: Any) {
        EmployeeDetailsViewController.name = EmployeeIDTF.text!
        let userID = EmployeeIDTF.text
        if(NetworkHelper.shared.isUserExists(userID:userID!)){
            let navCon2 = storyboard?.instantiateViewController(withIdentifier: "ClockInOutVCNavCon")
            navCon2?.modalPresentationStyle = .fullScreen
            
            self.present(navCon2!, animated: true, completion: nil)
        }
        else{
            let message = "Invaild User ID"
            let title = "Please enter a valid User ID"
            let action = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
            let actions:[UIAlertAction]=[action]
       self.present(AlertHelper.shared.presentAlert(title:title,message:message,actions:actions), animated: true)
    
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Enter ID or Scan your Bar Code"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(Back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        // Do any additional setup after loading the view.
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let clockinout = segue.destination as! ClockInOutViewController
//        clockinout.name = EmployeeIDTF.text
//    }
    @objc func Back(){
        self.dismiss(animated: true, completion: nil)
    }
    @objc func cancel(){
       // self.dismiss(animated: true, completion: nil)
        EmployeeIDTF.text = ""
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
