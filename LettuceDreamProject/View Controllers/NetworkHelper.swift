//
//  NetworkHelper.swift
//  LettuceDreamProject
//
//  Created by Student on 4/29/20.
//  Copyright © 2020 Bearcat Coders. All rights reserved.
//

import Foundation


import Foundation
import UIKit
struct NetworkHelper {
    
    private var API_Base = "localhost:8080"
    private static var _shared:NetworkHelper!
    
    static var shared:NetworkHelper {         // To access this anywhere, in the app just write NetworkHelper.shared
        if _shared == nil {
            _shared = NetworkHelper()
        }
        return _shared
    }
    
    func isUserExists(userID:String) -> Bool{
        var result = false;
        let url = URL(string: "http://localhost:8080/api/v1/user/verify")
        let sem = DispatchSemaphore(value: 0)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        let postString = "userId=\(userID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            defer { sem.signal() }
            if let error = error {
                print("Error took place \(error)")
            }
            // Convert HTTP Response Data to a String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print(dataString)
                if(self.handleData(responeData: dataString)){
                    result = true
                }
            }
        }
        task.resume()
        sem.wait(timeout: .distantFuture)
        return result
    }

    private func handleData(responeData : String )-> Bool{
        var result:Bool = false
        let data = Data(responeData.utf8)
        
        do {
            // make sure this JSON is in the format we expect
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                // try to read out a string array
                
                if let user_Status = json["isUserExist"] as? Int {
                    result = true
                }
                if let error_description = json["error_description"] as? String {
                    result = false
                }
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
            result = false
        }
        return result
    }
    
    
}
