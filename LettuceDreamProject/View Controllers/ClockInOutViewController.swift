//
//  ClockInOutViewController.swift
//  LettuceDreamProject
//
//  Created by Rohith Bharadwaj on 01/03/20.
//  Copyright © 2020 Bearcat Coders. All rights reserved.
//

import UIKit

class ClockInOutViewController: UIViewController {
    var name:String!
    var emp:Employee!
    let myDatePicker: UIDatePicker = UIDatePicker()
    
    @IBOutlet weak var EmployeeName: UILabel!
    @IBAction func clockInButton(_ sender: Any) {
        let alert = UIAlertController(title: "\(String(EmployeeName.text!))", message: "Clocked In Created Successfully on ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
//        alert.view.addSubview(myDatePicker)
//        let selectAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
//            print("Selected Date: \(self.myDatePicker.date)")
//        })
//        alert.addAction(selectAction)
        present(alert, animated: true)
    }
    
    @IBAction func clockOutButton(_ sender: Any) {
         let alert = UIAlertController(title: "\(String(EmployeeName.text!))", message: "Clocked Out Created Successfully on ", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                present(alert, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Click Clock In and Out"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(Back))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        //EmployeeName.text = name!
        EmployeeName.text = EmployeeDetailsViewController.name
        
        // Do any additional setup after loading the view.
    }
    @objc func Back(){
        self.dismiss(animated: true, completion: nil)
        
    }
    @objc func cancel(){
       // self.dismiss(animated: true, completion: nil)
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
