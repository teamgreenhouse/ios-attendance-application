//
//  ViewController.swift
//  LettuceDreamProject
//
//  Created by Rohith Bharadwaj on 13/02/20.
//  Copyright © 2020 Bearcat Coders. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBAction func attendanceButton(_ sender: Any) {
        let navCon1 = storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailsVCNavCon")
        navCon1?.modalPresentationStyle = .fullScreen
        self.present(navCon1!, animated: true, completion: nil)

    }
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.attendanceButton.layer.cornerRadius = 20
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        dateLabel.text = "\(hour):\(minutes):\(seconds)  \(day)-\(month)-\(year)"
       // dateLabel.text = "\(date)"
        
   
    }

    
    

}
